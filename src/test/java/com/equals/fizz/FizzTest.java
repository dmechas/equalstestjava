package com.equals.fizz;

import org.junit.Assert;
import org.junit.Test;

public class FizzTest  {
	
	@Test
	public void testMultiple3() {
		Fizz f = new Fizz();

		Assert.assertTrue(f.checkMultiple3(3));
		Assert.assertTrue(f.checkMultiple3(6));
		Assert.assertTrue(f.checkMultiple3(9));
		
		Assert.assertFalse(f.checkMultiple3(1));
		Assert.assertFalse(f.checkMultiple3(2));
		Assert.assertFalse(f.checkMultiple3(5));
	}

	@Test
	public void testMultiple5() {
		Fizz f = new Fizz();

		Assert.assertTrue(f.checkMultiple5(5));
		Assert.assertTrue(f.checkMultiple5(10));
		Assert.assertTrue(f.checkMultiple5(15));
		
		Assert.assertFalse(f.checkMultiple5(1));
		Assert.assertFalse(f.checkMultiple5(2));
		Assert.assertFalse(f.checkMultiple5(6));
	}
	@Test
	public void testStep1Zero() {
		Fizz f = new Fizz();
		String step1Str = f.run(0, 1);
		
		String expected = "fizzbuzz 1 ";
		Assert.assertEquals(expected, step1Str);
	}

	@Test
	public void testStep1Single() {
		Fizz f = new Fizz();
		String step1Str = f.run(1, 1);
		
		String expected = "1 ";
		Assert.assertEquals(expected, step1Str);
	}

	@Test
	public void testStep1() {
		Fizz f = new Fizz();
		String step1Str = f.run(1, 20);
		
		String expected = "1 2 fizz 4 buzz fizz 7 8 fizz buzz 11 fizz 13 14 fizzbuzz 16 17 fizz 19 buzz ";
		Assert.assertEquals(expected, step1Str);
	}
}
