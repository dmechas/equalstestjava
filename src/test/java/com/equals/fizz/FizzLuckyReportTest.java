package com.equals.fizz;

import org.junit.Assert;
import org.junit.Test;

public class FizzLuckyReportTest  {
	
	@Test
	public void testStepZero() {
		String stepStr = new FizzLuckyReport().run(0, 1);
		
		String expected = "fizzbuzz 1 \n"
				+ "fizz: 0\n"
				+ "buzz: 0\n"
				+ "fizzbuzz: 1\n"
				+ "lucky: 0\n"
				+ "integer: 1\n";
		Assert.assertEquals(expected, stepStr);
	}
	
	@Test
	public void testStepSingle() {
		String stepStr = new FizzLuckyReport().run(1, 1);
		
		String expected = "1 \n"
				+ "fizz: 0\n"
				+ "buzz: 0\n"
				+ "fizzbuzz: 0\n"
				+ "lucky: 0\n"
				+ "integer: 1\n";
		Assert.assertEquals(expected, stepStr);
	}


	@Test
	public void testStep3() {
		String stepStr = new FizzLuckyReport().run(1, 20);
		
		String expected = "1 2 lucky 4 buzz fizz 7 8 fizz buzz 11 fizz lucky 14 fizzbuzz 16 17 fizz 19 buzz \n"
				+ "fizz: 4\n"
				+ "buzz: 3\n"
				+ "fizzbuzz: 1\n"
				+ "lucky: 2\n"
				+ "integer: 10\n";
		Assert.assertEquals(expected, stepStr);
	}
}
