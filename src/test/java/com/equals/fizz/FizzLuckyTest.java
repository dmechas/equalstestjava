package com.equals.fizz;

import org.junit.Assert;
import org.junit.Test;

public class FizzLuckyTest  {
	
	@Test
	public void testStepZero() {
		String stepStr = new FizzLucky().run(0, 1);
		String expected = "fizzbuzz 1 ";
		Assert.assertEquals(expected, stepStr);
	}
	
	@Test
	public void testStepSingle() {
		String stepStr = new FizzLucky().run(1, 1);
		String expected = "1 ";
		Assert.assertEquals(expected, stepStr);
	}


	@Test
	public void testStep2() {
		String stepStr = new FizzLucky().run(1, 20);
		
		String expected = "1 2 lucky 4 buzz fizz 7 8 fizz buzz 11 fizz lucky 14 fizzbuzz 16 17 fizz 19 buzz ";
		Assert.assertEquals(expected, stepStr);
	}
}
