package com.equals.fizz;

public class Fizz {

	private final String FIZZ = "fizz";
	private final String BUZZ = "buzz";
	private final String FIZZBUZZ = "fizzbuzz";
	
	public Fizz() {
	}
	

	public String run(Integer start, Integer end) {
		StringBuilder sb = new StringBuilder();
	    for( Integer i = start ; i <= end ; i++ ) {
	        sb.append(new String[]{ i.toString(), FIZZ, BUZZ, FIZZBUZZ }[ ( checkMultiple3(i) ? 1: 0 ) + ( checkMultiple5(i) ? 2 : 0 ) ] + " ");
	    }
		return sb.toString();
	}

	public static void main(String[] args) {
		Fizz f = new Fizz();
		System.out.println(f.run(1, 20));
	}
	
	
	public boolean checkMultiple3(Integer value) {
		return checkMultipleOf(value, 3);
	}

	public boolean checkMultiple5(Integer value) {
		return checkMultipleOf(value, 5);
	}
	
	private boolean checkMultipleOf(Integer value, Integer ref) {
		return value%ref == 0;
	}
}
