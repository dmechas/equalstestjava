package com.equals.fizz;

public class FizzLucky {

	private final String FIZZ = "fizz";
	private final String BUZZ = "buzz";
	private final String FIZZBUZZ = "fizzbuzz";
	private final String LUCKY = "lucky";
	
	public FizzLucky() {
	}

	public String run(Integer start, Integer end) {
		StringBuilder sb = new StringBuilder();
	    for( Integer i = start ; i <= end ; i++ ) {
	    	if (String.valueOf(i).contains("3")) {
				sb.append(LUCKY);
			} else {
				sb.append(new String[]{ String.valueOf(i), FIZZ, BUZZ, FIZZBUZZ }[ ( checkMultiple3(i) ? 1: 0 ) + ( checkMultiple5(i) ? 2 : 0 ) ]);
			}
			sb.append(" ");
	    }
		return sb.toString();
	}

	public static void main(String[] args) {
		FizzLucky f = new FizzLucky();
		System.out.println(f.run(1, 20));
	}
	
	
	public boolean checkMultiple3(Integer value) {
		return checkMultipleOf(value, 3);
	}

	public boolean checkMultiple5(Integer value) {
		return checkMultipleOf(value, 5);
	}
	
	private boolean checkMultipleOf(Integer value, Integer ref) {
		return value%ref == 0;
	}
}
