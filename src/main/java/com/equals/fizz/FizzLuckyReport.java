package com.equals.fizz;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

public class FizzLuckyReport {

	private final String FIZZ = "fizz";
	private final String BUZZ = "buzz";
	private final String FIZZBUZZ = "fizzbuzz";
	private final String LUCKY = "lucky";
	private final String INTEGER = "integer";
	private final DataReport dataReport = new DataReport();
	private final String LUCKY_DIGIT = "3";
	
	public FizzLuckyReport() {
	}

	public String run(Integer start, Integer end) {
		StringBuilder sb = new StringBuilder();
	    for( Integer i = start ; i <= end ; i++ ) {
	    	String value;
	    	if (String.valueOf(i).contains(LUCKY_DIGIT)) {
	    		value = LUCKY;
			} else {
				value = (new String[]{ String.valueOf(i), FIZZ, BUZZ, FIZZBUZZ }[ ( checkMultiple3(i) ? 1: 0 ) + ( checkMultiple5(i) ? 2 : 0 ) ]);
			}
			dataReport.reportintValue(value);
			sb.append(value + " ");
	    }
	    
	    sb.append("\n");
		dataReport.generateReport(sb);
		return sb.toString();
	}

	public static void main(String[] args) {
		FizzLuckyReport f = new FizzLuckyReport();
		System.out.println(f.run(1, 20));
	}
	
	
	public boolean checkMultiple3(Integer value) {
		return checkMultipleOf(value, 3);
	}

	public boolean checkMultiple5(Integer value) {
		return checkMultipleOf(value, 5);
	}
	
	private boolean checkMultipleOf(Integer value, Integer ref) {
		return value%ref == 0;
	}
	
	private class DataReport {
		private Map<String, Integer> valuesMap = new LinkedHashMap<String, Integer>();
		
		public DataReport() {
			valuesMap.put(FIZZ, 0);
			valuesMap.put(BUZZ, 0);
			valuesMap.put(FIZZBUZZ, 0);
			valuesMap.put(LUCKY, 0);
			valuesMap.put(INTEGER, 0);
		}

		public void generateReport(StringBuilder sb) {
			for (Entry<String, Integer> entry : valuesMap.entrySet()) {
				sb.append(String.format("%s: %d\n", entry.getKey(), entry.getValue()));
			}
		}

		private void reportintValue(String value) {
			if (valuesMap.containsKey(value)) {
				int count = valuesMap.get(value);
				valuesMap.put(value, ++count);
			} else {
				int count = valuesMap.get(INTEGER);
				valuesMap.put(INTEGER, ++count);
			}
		}
	}
}
